package com.company;

public class BojaKotaca {
    private String boja;

    public BojaKotaca(String boja){this.setBojaKotaca(boja);}

    public String getBojaKotaca() {
        return this.boja;
    }

    public void setBojaKotaca(String boja) {
        this.boja = boja;
    }

}
