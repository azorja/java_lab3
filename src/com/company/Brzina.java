package com.company;

public class Brzina {
    public double brzina(double metri,double sekunde){
        double izracun= metri/sekunde;
        return (izracun*3.6);
    }
    public double brzina(double metri,double sekunde,double milje) {
        double izracun = metri / sekunde;
        double miljeKoeficijent = 1.61;
        return ((izracun * 3.6) * miljeKoeficijent);
    }
}
