package com.company;

public class Mountain extends Bicycle{
    String color;
    String suspension;
    private BojaKotaca bojaKotaca;
    Brzina bicikla;


    public Mountain(String marka, String velicina, Integer duzina, String color, String suspension,BojaKotaca bojaKotaca) {
        super(marka, velicina, duzina);
        this.color=color;
        this.suspension = suspension;
    }

    @Override
    public void printaj() {
        super.printaj();
        System.out.println(color+" "+suspension+" "+bojaKotaca);
    }

    public double kmh(double metri,double sekunde){
        bicikla=new Brzina();
        return (bicikla.brzina(metri,sekunde));
    }



}
